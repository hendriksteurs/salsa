<section .section>
  <div .container>
    <h1 .title .is-3>
      $maybe _ <- mPartyUuid
        Edit Party
      $nothing
        Submit Party

    <form .form
      enctype="multipart/form-data"
      method=post
      action=@{AccountR AccountSubmitPartyR}>

      <div .field>
        <label .label>
          Title
        <div .control>
          <input .input
            type=text
            name="title"
            value=#{tv partyTitle}
            required>

      <div .field>
        <label .label>
          Day
        <div .control>
          <input .input
            type=date
            required
            value=#{mt "%F" partyDay}
            name="day"
            :disableDateSetting:disabled>
        $if disableDateSetting
          <p .help>
            You can no longer edit the day for the party.
            If you were looking to do that, you can either cancel this party and organise another, or duplicate this party to re-organise it.

      <div .field>
        <label .label>
          Address
        <div .control>
          <input .input
            type=text
            value=#{maybe "" placeQuery mPlace}
            required
            name="address">

      <div .field>
        <label .label>
          Description
        <div .control>
          <textarea .textarea
            name="description">
            #{mtv partyDescription}

      <div .field>
        <label .label>
          Start
        <div .control>
          <input .input
            type=time
            value=#{mmt "%H:%M" partyStart}
            name="start">

      <div .field>
        <label .label>
          Homepage
        <div .control>
          <input .input
            type=url
            value=#{mtv partyHomepage}
            name="homepage">

      <div .field>
        <label .label>
          Price
        <div .control>
          <input .input
            type=text
            value=#{mtv partyPrice}
            name="price">
        <p .help>
          Make sure to write FREE if the party is free.

      <div .field>
        <label .label>
          Poster
        <div .control>
          <div #posterInput .file .has-name>
            <label .file-label>
              <input .input .file-input .input name="poster" type="file">
              <span .file-cta>
                <span .file-icon>
                  <i .fas .fa-upload>
                <span .file-label>
                  Choose a poster image
              <span .file-name>
                No poster selected

      $maybe (posterKey, posterWidget) <- mPosterTup
        <div .field>
          <div .control>
            <input .input name="poster-key" type="hidden" value=#{renderCASKey posterKey}>
        ^{posterWidget}


      $maybe partyUuid <- mPartyUuid
        <div .field>
          <div .control>
            <input .input name="uuid" type="hidden" value=#{uuidText partyUuid}>

      <div .field>
        <div .control>
          ^{token}

      <div .field>
        <div .control>
          <div .buttons>
            <button .button .is-dark type="submit">
              Submit
            <a .button href=@{AccountR AccountPartiesR}>
              Back
            $maybe partyUuid <- mPartyUuid
              <a .button href=@{PartyR partyUuid}>
                Public party profile
